# `Al Mundo App` — Aplicación de prueba para al mundo Argentina

## Comenzar

Descargar el archivo ZIP o clonar con git

### Pre-requisitos

1. Necesitas clonar este proyecto, puedes hacer ```git clone git@gitlab.com:lmachillanda591/almundo.git```
2. También necesitas tener instalado node, puedes descargarlo e instalarlo dirigiendote[aquí][node].
3. Además necesitas descargar e instalar mongoDB, puedes conseguirlo justo [aquí][mongo]
3. Y necesitas descargar e instalar Bower, puedes conseguirlo justo [aquí][bower]

### Instalar Dependencias

1. Corre ```bower install```
2. Corre ```npm install```

### Para minificación y concatenación

Cada vez que hagas un cambio a los archivos de la carpeta hotel, o admin, y al archivo app.css debes hacer:

1. correr ```gulp js```
2. correr ```gulp css```
2. correr ```gulp html```

ó ```gulp html js css``` para hacer todo junto.

Puedes modificar el archivo que está en la carpeta raíz llamado `gulpfile.js` para ajustarlo a tu gusto.


### Levantar la aplicación

1. Correr ```npm start```
2. ir a [http://localhost:8000][local]

## Directorio Layout

```
app/                    --> Archivos fuentes de la aplicación
  app.css               --> Estilos por defecto
  app.min.css           --> Estilos minificados
  bower_components      --> Packages de Bower
  admin/                --> La vista y lógica del panel administrativo
    min/
      admin.html            --> el template minificado
      admin.min.js          --> la logica del panel minificada
    admin.html            --> el template parcial
    admin.js              --> la logica del panel
  hotel/                --> La vista y lógica de los hoteles
    min/
      hotel.html            --> el template minificado
      hotel.min.js          --> lógica de los hoteles minificada
    hotel.html            --> el template parcial
    hotel.js              --> la lógica de los hoteles
  app.js                --> El modulo principal de la aplicación
  main.js               --> todos los controladores concatenados
  libs/                 --> Dónde irían librerías externas
  index.html            --> el html principal de la aplicación
karma.conf.js         --> configurar archivo para correr pruebas unitarias

```

[node]: https://nodejs.org/
[mongo]: https://www.mongodb.com/es
[local]: http://localhost:8000
[bower]: https://bower.io/