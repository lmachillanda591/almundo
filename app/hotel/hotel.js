'use strict';

angular.module('myApp.hotel', ['ngRoute'])

.controller('hotelCtrl', ['$scope', '$http', '$rootScope', 'hotelFactory', '$filter', 'orderByFilter', function($scope, $http, $rootScope, hotelFactory, $filter, orderBy) {
  
  $scope.openBox = {star: true, text:true, night:true, filter: true, map: true};

  $scope.getHotels = function (callback) {
    // Cuando se cargue la página, pide del API todos los hoteles
    $scope.nameOrder = 'Más relevante';
    hotelFactory.getHotels().success(function(data) {
      $scope.hotels = data;
      $scope.countStars = {five: 0, four: 0, three: 0, two: 0, one: 0, total: 0};

      $scope.hotels.forEach(hotel => {
        if (hotel.stars == 5) {$scope.countStars.five = $scope.countStars.five + 1; $scope.countStars.total = $scope.countStars.total + 1}
        else if (hotel.stars == 4) {$scope.countStars.four = $scope.countStars.four + 1; $scope.countStars.total = $scope.countStars.total + 1}
        else if (hotel.stars == 3) {$scope.countStars.three = $scope.countStars.three + 1;  $scope.countStars.total = $scope.countStars.total + 1}
        else if (hotel.stars == 2) {$scope.countStars.two = $scope.countStars.two + 1;  $scope.countStars.total = $scope.countStars.total + 1}
        else {$scope.countStars.one = $scope.countStars.one + 1;  $scope.countStars.total = $scope.countStars.total + 1};
      }) //Contar estrellas

      if (callback)
        callback(); //Buscar hoteles y hacer filtro
    })
    .error(function(data) {
      if (callback)
        callback();
      console.log('Error: ' + data);
    });
  }

  $scope.getHotels();

  $scope.moveImg = function (dir, a, index) {
    let temp;
    if (dir == "up") {
      return temp = (a == $scope.hotels[index].imgUrl.length-1 ? 0 : a + 1)
    }
    else if (dir == "down") {
      return temp = (a == 0 ? $scope.hotels[index].imgUrl.length-1 : a - 1)
    }
  } //Move image in image caroussel

  $scope.searchHotel = function (hotel) {
    $scope.getHotels(filter);
    function filter(){
      hotel ? ($scope.hotels = $filter('filter')($scope.hotels, {name: hotel})):($scope.getHotels())
    }
  }

  let changeCheckbox = function() {    
    $('input[type="checkbox"]').on('change', function() {
      $('input[type="checkbox"]').not(this).prop('checked', false);
      $('input[type="checkbox"]').not(this).prop('disabled', false);
      $(this).prop('disabled', true);
    });
  }

  $scope.searchStar = function (star) {
    $scope.getHotels(filter);
    changeCheckbox()
    function filter() {
      star ? ($scope.hotels = $filter('filter')($scope.hotels, {stars: star})):($scope.getHotels())
    }
  }

  $scope.orderHotels = function(data, name) {
    $scope.nameOrder = name;
    $scope.hotels = orderBy($scope.hotels, data, true);
  }

}])