'use strict';

angular.module('myApp.admin', ['ngRoute'])

.controller('adminCtrl', ['$scope', '$http', 'hotelFactory', 'orderByFilter', function($scope, $http, hotelFactory, orderBy) {
  
  $scope.formData = {imgUrl: [], recommended: false, stars: 1, payDestiny:false, payDues:false, price: 0}; //recommended and stars default.
  // Cuando se cargue la página, pide del API todos los hoteles
  hotelFactory.getHotels().success(function(data) {
    $scope.hotels = orderBy(data, '-created_at'); //Ordernar por fecha de creacion descendente.
  })
  .error(function(data) {
    console.log('Error: ' + data);
  });

  // Cuando se añade un nuevo hotel, manda el texto a la API
  $scope.createHotel = function(){
    $scope.formData.created_at = new Date()
    hotelFactory.createHotel($scope.formData).success(function(data) {
      $scope.formData = {imgUrl: [], recommended: false, stars: 1, payDestiny:false, payDues:false, price: 0};
      $scope.hotels = orderBy(data, '-created_at'); //Ordernar por fecha de creacion descendente.
    })
    .error(function(data) {
      console.log('Error:' + data);
    });
  };

  // Borra un TODO despues de checkearlo como acabado
  $scope.deleteHotel = function(id) {
    hotelFactory.deleteHotel(id).success(function(data) {
      $scope.hotels = orderBy(data, '-created_at'); //Ordernar por fecha de creacion descendente.
    })
    .error(function(data) {
      console.log('Error:' + data);
    });
  };

// Working with images
  $scope.submitImg = function(data) {
    if (data) {
      let file = new File([data], "name");
      let reader = new window.FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = function() {
        $scope.formData.imgUrl.push(reader.result);
      }
    }
  }; //Submit image in array

  $scope.moveImg = function (dir, a, index) {
    let temp;
    if (dir == "up") {
      return temp = (a == $scope.hotels[index].imgUrl.length-1 ? 0 : a + 1)
    }
    else {
      return temp = (a == 0 ? $scope.hotels[index].imgUrl.length-1 : a - 1)
    }
  }

  $scope.removeImg = function(index){
    $scope.formData.imgUrl.splice(index, 1);
  }

}]);