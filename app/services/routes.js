'use strict';

angular.module('myApp.routes', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/list_hotels', {
    templateUrl: 'hotel/min/hotel.html',
    controller: 'hotelCtrl'
  });
  $routeProvider.when('/admin', {
    templateUrl: 'admin/min/admin.html',
    controller: 'adminCtrl'
  });
}])